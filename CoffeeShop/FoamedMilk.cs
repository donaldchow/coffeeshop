﻿namespace CoffeeShop
{
    public class FoamedMilk : IMilk
    {
        public ICupSize CupSize { get; set; }

        public decimal Cost()
        {
            return 0.05m + CupSize.Cost() + 0.5m;
        }
    }
}