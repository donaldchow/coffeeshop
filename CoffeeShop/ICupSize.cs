﻿namespace CoffeeShop
{
    public interface ICupSize
    {
        decimal Cost();
    }

    public class CupSizeFactory
    {
        public static ICupSize Create(bool isLarge)
        {
            if (isLarge)
                return new LargeCup();
            return new SmallCup();
        }
    }
}
