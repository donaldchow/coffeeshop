﻿namespace CoffeeShop
{
    public class Coffee : ICoffee
    {
        public Coffee()
        {
            NumberOfSugars = 0m;
        }
        
        public IMilk Milk { get; set; }
        public bool HasSprinkles { get; set; }
        public decimal NumberOfSugars { get; set; }
        public ICupSize CupSize { get; set; }
        public decimal CalculateCost()
        {
            return 1.50m + CupSize.Cost() + (0.10m * NumberOfSugars) + Milk.Cost();
        }

        public bool HasFlavouring { get; set; }

        public Coffee WithSprinkles(bool hasSprinkles)
        {
            HasSprinkles = hasSprinkles;
            return this;
        }

        public Coffee HasSugar(uint numOfSugar)
        {
            NumberOfSugars = numOfSugar;
            return this;
        }
    }
}
