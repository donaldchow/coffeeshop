﻿namespace CoffeeShop
{
    public class Tea : ITea
    {
        public IMilk Milk { get; set; }
        public decimal NumberOfSugars { get; set; }
        public ICupSize CupSize { get; set; }
        public decimal CalculateCost()
        {
            return 1.00m + CupSize.Cost() + (0.10m * NumberOfSugars) + Milk.Cost();
        }
    }
}
