﻿namespace CoffeeShop
{
    public interface IMilk
    {
        ICupSize CupSize { get; set; }
        decimal Cost();
    }

    public class MilkFactory
    {
        public static IMilk Create(ICupSize cupSize)
        {
            return new NoMilk {CupSize = cupSize};
        }
    }

    public static class MilkExtension
    {
        public static IMilk HasMilk(this IMilk milk, bool hasMilk)
        {
            return hasMilk
                ? (IMilk) new NoMilk {CupSize = milk.CupSize}
                : new Milk {CupSize = milk.CupSize};
        }

        public static IMilk IsFoamed(this IMilk milk, bool isFoamed)
        {
            return isFoamed
                ? (IMilk) new FoamedMilk {CupSize = milk.CupSize}
                : new Milk {CupSize = milk.CupSize};
        }
    }
}
