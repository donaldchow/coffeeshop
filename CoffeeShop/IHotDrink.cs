﻿using System.Dynamic;

namespace CoffeeShop
{
    public interface IHotDrink
    {
        IMilk Milk { get; set; }
        decimal NumberOfSugars { get; set; }
        ICupSize CupSize { get; set; }
        decimal CalculateCost();
    }
}