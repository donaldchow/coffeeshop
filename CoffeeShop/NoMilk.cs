﻿namespace CoffeeShop
{
    public class NoMilk :IMilk
    {
        public ICupSize CupSize { get; set; }

        public decimal Cost()
        {
            return 0m;
        }
    }
}