﻿namespace CoffeeShop
{
    public class HotChocolate : IHotChocolate
    {
        public HotChocolate()
        {
            HasSprinkles = true;
        }

        public IMilk Milk { get; set; }
        public decimal NumberOfSugars { get; set; }
        public ICupSize CupSize { get; set; }
        public decimal CalculateCost()
        {
            return 1.50m + CupSize.Cost() + (0.10m * NumberOfSugars) + Milk.Cost();
        }

        public bool HasSprinkles { get; set; }
    }
}