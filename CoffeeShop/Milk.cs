﻿namespace CoffeeShop
{
    public class Milk : IMilk
    {
        public ICupSize CupSize { get; set; }

        public decimal Cost()
        {
            return 0.30m + CupSize.Cost() + 0.5m;
        }
    }
}