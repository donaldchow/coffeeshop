﻿namespace CoffeeShop
{
    public interface IHotChocolate : IHotDrink
    {
        bool HasSprinkles { get; set; }
    }
}