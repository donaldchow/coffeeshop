﻿namespace CoffeeShop
{
    public interface ICoffee : IHotDrink
    {
        bool HasFlavouring { get; set; }
        bool HasSprinkles { get; set; }
    }
}