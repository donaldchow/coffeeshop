﻿using System;

namespace CoffeeShop.Orders
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Large? (true/false)");
            var isLarge = bool.Parse(Console.ReadLine());

            Console.WriteLine("Milk? (true/false)");
            var hasMilk = bool.Parse(Console.ReadLine());

            Console.WriteLine("How many sugars?");
            var numSugars = uint.Parse(Console.ReadLine());

            Console.WriteLine("Foam the milk? (true/false)");
            var milkIsFoamed = bool.Parse(Console.ReadLine());

            Console.WriteLine("Add chocolate sprinkles? (true/false)");
            var hasSprinkles = bool.Parse(Console.ReadLine());

            // Perform some validation
            var cupSize = CupSizeFactory.Create(isLarge);

            var coffee = new Coffee
            {
                CupSize = cupSize,
                Milk = MilkFactory.Create(cupSize).HasMilk(hasMilk).IsFoamed(milkIsFoamed)
            }.WithSprinkles(hasSprinkles).HasSugar(numSugars);
            Console.WriteLine("Cost is $" + coffee.CalculateCost());
            Console.ReadKey(true);
;
        }
    }
}
