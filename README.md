# CoffeeShop - Readme #



### What is this repository for? ###

* Coffee Shop Coding Excercise used to show SOLID principles
* This code was originally used in a Brown Bag to show how to migrate from 
* To calculate the following 
	* 


### How do I get set up? ###

* This solution requires .Net Core 2.0
* 
* 

### How to run tests ###
* 


### Assumptions ###

* 

### Original Code ###
The following is the original code that is the basis of the coding exercise.  The idea is to get the code to a production ready state.

using System;

namespace CoffeeConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Large? (true/false)");
            bool isLarge = bool.Parse(Console.ReadLine());

            Console.WriteLine("Milk? (true/false)");
            bool hasMilk = bool.Parse(Console.ReadLine());

            Console.WriteLine("How many sugars?");
            int numSugars = int.Parse(Console.ReadLine());

            Console.WriteLine("Foam the milk? (true/false)");
            bool milkIsFoamed = bool.Parse(Console.ReadLine());

            Console.WriteLine("Add chocolate sprinkles? (true/false)");
            bool hasSprinkles = bool.Parse(Console.ReadLine());

            Coffee exampleCoffee = new Coffee
            {
                IsLarge = isLarge,
                HasMilk = hasMilk,
                NumSugars = numSugars,
                MilkIsFoamed = milkIsFoamed,
                HasSprinkles = hasSprinkles
            };

            double cost = new CoffeeCalculator().CalculateCost(exampleCoffee);
            Console.WriteLine("Cost is $" + cost);
            Console.ReadKey(true);

        }
    }

    public class Coffee
    {
        public bool IsLarge;
        public bool HasMilk;
        public int NumSugars;
        public bool MilkIsFoamed;
        public bool HasSprinkles;
    }

    public class CoffeeCalculator
    {
        public double CalculateCost(Coffee coffee)
        {
            // Start with basic Expresso
            double x = 1.50;

            //Milk 25c or 30c for large
            if (coffee.HasMilk & coffee.IsLarge)
                x += .30;

            if (coffee.HasMilk & !coffee.IsLarge)
                x += .25;

            // Calc sugar @ 10c each
            x += (0.10 * coffee.NumSugars);

            // Milk Foaming 5c
            if (coffee.MilkIsFoamed)
                x += .05;

            // Add service charge of $1.30 per order    
            x += 1.30;

            return x;

        }
    }
}
