﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeShop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class FoamedMilkTests
    {
        [TestMethod]
        public void Cost_SmallCup_Test()
        {
            var foamedMilk = new FoamedMilk {CupSize = new SmallCup()};

            var actual = foamedMilk.Cost();
            Assert.AreEqual(0.8m, actual);
        }

        [TestMethod]
        public void Cost_LargeCup_Test()
        {
            var foamedMilk = new FoamedMilk {CupSize = new LargeCup()};

            var actual = foamedMilk.Cost();
            Assert.AreEqual(1.55m, actual);
        }
    }
}