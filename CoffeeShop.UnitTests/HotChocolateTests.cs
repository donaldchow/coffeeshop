﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeShop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class HotChocolateTests
    {
        [TestMethod]
        public void CalculateCost_PlainHotChocolateNoSugar_Test()
        {
            var cupSize = CupSizeFactory.Create(false);
            var hotChocolate = new HotChocolate
            {
                CupSize = cupSize,
                Milk = MilkFactory.Create(cupSize).HasMilk(false).IsFoamed(false),
                NumberOfSugars = 0
            };
            var cost = hotChocolate.CalculateCost();
            Assert.AreEqual(2.80M, cost);
        }

        [TestMethod]
        public void CalculateCost_HotChocolateOneSugar_Test()
        {
            var cupSize = CupSizeFactory.Create(false);
            var hotChocolate = new HotChocolate
            {
                CupSize = cupSize,
                Milk = MilkFactory.Create(cupSize).HasMilk(false).IsFoamed(false),
                NumberOfSugars = 1,
                HasSprinkles = true
            };
            var cost = hotChocolate.CalculateCost();
            Assert.AreEqual(2.90M, cost);
        }

    }
}