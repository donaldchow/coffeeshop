﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeShop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class MilkTests
    {
        [TestMethod]
        public void Cost_SmallCup_Test()
        {
            var foamedMilk = new Milk { CupSize = new SmallCup() };

            var actual = foamedMilk.Cost();
            Assert.AreEqual(1.05m, actual);
        }

        [TestMethod]
        public void Cost_LargeCup_Test()
        {
            var foamedMilk = new Milk { CupSize = new LargeCup() };

            var actual = foamedMilk.Cost();
            Assert.AreEqual(1.8m, actual);
        }
    }
}