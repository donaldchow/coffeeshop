﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeShop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class NoMilkTests
    {
        [TestMethod]
        public void Cost_SmallCup_Test()
        {
            var foamedMilk = new NoMilk { CupSize = new SmallCup() };

            var actual = foamedMilk.Cost();
            Assert.AreEqual(0m, actual);
        }

        [TestMethod]
        public void Cost_LargeCup_Test()
        {
            var foamedMilk = new NoMilk { CupSize = new LargeCup() };

            var actual = foamedMilk.Cost();
            Assert.AreEqual(0m, actual);
        }
    }
}