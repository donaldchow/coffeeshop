﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeShop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class CoffeeTests
    {
        [TestMethod]
        public void CalculateCost_EspressoNoSugar_Test()
        {
            var cupSize = CupSizeFactory.Create(false);
            var coffee = new Coffee
            {
                CupSize = cupSize,
                Milk = MilkFactory.Create(cupSize).HasMilk(false).IsFoamed(false),
                NumberOfSugars = 0
            }.WithSprinkles(false).HasSugar(0);
            var cost = coffee.CalculateCost();
            Assert.AreEqual(2.80M, cost);
        }

        [TestMethod]
        public void CalculateCost_EspressoOneSugar_Test()
        {
            var cupSize = CupSizeFactory.Create(false);
            var coffee = new Coffee
            {
                CupSize = cupSize,
                Milk = MilkFactory.Create(cupSize).HasMilk(false).IsFoamed(false),
                NumberOfSugars = 0
            }.WithSprinkles(false).HasSugar(1);
            var cost = coffee.CalculateCost();
            Assert.AreEqual(2.90M, cost);
        }

        [TestMethod]
        public void CalculateCost_CapacinnoOneSugar_Test()
        {
            var cupSize = CupSizeFactory.Create(false);
            var coffee = new Coffee
            {
                CupSize = cupSize,
                Milk = MilkFactory.Create(cupSize).HasMilk(true).IsFoamed(true),
                NumberOfSugars = 0
            }.WithSprinkles(true).HasSugar(1);
            var cost = coffee.CalculateCost();
            Assert.AreEqual(2.65M, cost);
        }
    }
}